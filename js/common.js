const getRandom = (num) => Math.ceil(Math.random() * num);

function currentDate() {
    const date = new Date();
    const hours = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
    const minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
    const seconds = date.getSeconds() < 10 ? `0${date.getSeconds()}` : date.getSeconds();
    return `${hours}:${minutes}:${seconds}`;
}

function createElement(htmlTag, htmlClass) {
    let $htmlTag = document.createElement(htmlTag);
    if (htmlClass) {
        $htmlTag.classList.add(htmlClass);
    }
    return $htmlTag;
}
export {getRandom,currentDate,createElement};