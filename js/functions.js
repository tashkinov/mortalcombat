import {
    logs
} from './logs.js';
import {
    currentDate,
    getRandom,
    createElement
} from './common.js';
import {
    soundLib
} from './sounds.js';

const $fightButton = document.querySelector('.button');
const $chat = document.querySelector('.chat');
const HIT = {
    head: 30,
    body: 25,
    foot: 20,
}
const ATTACK = ['head', 'body', 'foot'];

export function winnerCheck(nameWinner, nameLoser) {
    let $winTitle = createElement('div', 'loseTitle');

    if (nameWinner || nameLoser) {
        $winTitle.innerHTML = nameWinner.name + ' wins';
        generateLogs('end', nameWinner, nameLoser)
    } else {
        $winTitle.innerHTML = 'draw';
        generateLogs('draw')
    }
    $arena.appendChild($winTitle)
}

export function playerAttack() {
    const attack = {};
    for (let item of $formFight) {
        if (item.checked && item.name === 'hit') {
            attack.value = getRandom(HIT[item.value]);
            attack.hit = item.value;
        }
        if (item.checked && item.name === 'defence') {
            attack.defence = item.value;
        }
        item.checked = false;
    }
    return attack;
}

export function enemyAttack() {
    const hit = ATTACK[getRandom(3) - 1];
    const defence = ATTACK[getRandom(3) - 1];
    return {
        value: getRandom(HIT[hit]),
        hit,
        defence,
    }
}

export function generateLogs(type, player1, player2, damage) {
    switch (type) {
        case 'start':
            const textStart = logs[type]
                .replace('[player1]', player1.name)
                .replace('[player2]', player2.name)
                .replace('[time]', currentDate());
            let elStart = `<p>${textStart}</p>`;
            $chat.insertAdjacentHTML('afterbegin', elStart);
            break;
        case 'end':
            const texEnd = logs[type][getRandom(logs[type].length) - 1]
                .replace('[playerLose]', player2.name)
                .replace('[playerWins]', player1.name);
            let elEnd = `<p>${texEnd}</p>`;
            $chat.insertAdjacentHTML('afterbegin', elEnd);
            break;
        case 'hit':
            const textHit = logs[type][getRandom(logs[type].length) - 1]
                .replace('[playerDefence]', player2.name)
                .replace('[playerKick]', player1.name);
            let elHit = `<p>${currentDate()} - ${textHit} -${damage} [${player2.hp}/100]</p>`;
            $chat.insertAdjacentHTML('afterbegin', elHit);
            break;
        case 'defence':
            const textDefence = logs[type][getRandom(logs[type].length) - 1]
                .replace('[playerDefence]', player1.name)
                .replace('[playerKick]', player2.name);
            let elDefence = `<p>${currentDate()} - ${textDefence}</p>`;
            $chat.insertAdjacentHTML('afterbegin', elDefence);
            break;
        case 'draw':
            $chat.insertAdjacentHTML('afterbegin', logs.draw);
            break;
    }
}

export function showResult() {
    if (player2.hp === 0 || player1.hp === 0) {
        $fightButton.disabled = true;
        createReloadButton();
        let typeEnd = logs.end;
        generateLogs(logs.end[getRandom(typeEnd.length)], player1, player2);
    }

    if (player1.hp === 0 && player1.hp < player2.hp) {
        winnerCheck(player2, player1);
    } else if (player2.hp === 0 && player2.hp < player1.hp) {
        winnerCheck(player1, player2);
    } else if (player1.hp === 0 && player2.hp === 0) {
        winnerCheck();
    }
}

function createReloadButton() {
    const $reloadButtonDiv = createElement('div', 'reloadWrap');
    const $reloadButton = createElement('button');
    $reloadButtonDiv.appendChild($reloadButton)
    $reloadButton.innerHTML = 'Restart';
    $arena.appendChild($reloadButtonDiv);

    $reloadButton.addEventListener('click', () => {
        window.location.reload()
    })
}

export function playSound(type, sex, alias) {
    let fileUrl;

    if (sex && type == 'gotHit') {
        switch (sex) {
            case 'male':
                fileUrl = soundLib.gotHit.sex.male[(getRandom(soundLib.gotHit.sex.male.length) - 1)];
                break;
            case 'female':
                fileUrl = soundLib.gotHit.sex.female[(getRandom(soundLib.gotHit.sex.female.length) - 1)];
                break;
            case 'machine':
                fileUrl = soundLib.gotHit.sex.machine[(getRandom(soundLib.gotHit.sex.machine.length) - 1)];
                break;
        }
    }
    if (sex && type == 'makeHit') {
        switch (sex) {
            case 'male':
                fileUrl = soundLib.makeHit.sex.male[(getRandom(soundLib.makeHit.sex.male.length) - 1)];
                break;
            case 'female':
                fileUrl = soundLib.makeHit.sex.female[(getRandom(soundLib.makeHit.sex.female.length) - 1)];
                break;
            case 'machine':
                fileUrl = soundLib.makeHit.sex.machine[(getRandom(soundLib.makeHit.sex.machine.length) - 1)];
                break;
        }

    }
    if (!sex && type != 'name' && type != 'wins') {
        switch (type) {
            case 'block':
                fileUrl = soundLib.block[(getRandom(soundLib.block.length) - 1)];
                break;
            case 'pounch':
                fileUrl = soundLib.pounch[(getRandom(soundLib.pounch.length) - 1)];
                break;
            case 'chosen':
                fileUrl = `source/sounds/chosen.mp3`;
                break;
            case 'vs':
                fileUrl = `source/sounds/vs.mp3`;
                break;
            case 'fight':
                fileUrl = `source/sounds/fight.mp3`;
        }
    }

    if (type == 'wins' || alias) {
        fileUrl = `source/character/${alias}/sounds/${type}.mp3`;
    }
    if (type == 'name' || alias) {
        fileUrl = `source/character/${alias}/sounds/${type}.mp3`;
    }

    return new Audio(fileUrl);
}
