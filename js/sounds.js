const soundLib = {
    block: [
        '/source/sounds/fight/block/block_0.mp3',
        '/source/sounds/fight/block/block_1.mp3',
        '/source/sounds/fight/block/block_2.mp3',
        '/source/sounds/fight/block/block_3.mp3',
    ],
    pounch: [
        'звук удара',
    ],
    gotHit: {
        sex: {
            female: [
                '/source/sounds/fight/female/get_hit_0.mp3',
                '/source/sounds/fight/female/get_hit_1.mp3',
                '/source/sounds/fight/female/get_hit_2.mp3',
                '/source/sounds/fight/female/get_hit_3.mp3',
                '/source/sounds/fight/female/get_hit_4.mp3',
                '/source/sounds/fight/female/get_hit_5.mp3',
                '/source/sounds/fight/female/get_hit_6.mp3',
                '/source/sounds/fight/female/get_hit_7.mp3',
                '/source/sounds/fight/female/get_hit_8.mp3',
            ],
            male: [
                '/source/sounds/fight/male/get_hit_0.mp3',
                '/source/sounds/fight/male/get_hit_1.mp3',
                '/source/sounds/fight/male/get_hit_2.mp3',
                '/source/sounds/fight/male/get_hit_3.mp3',
                '/source/sounds/fight/male/get_hit_4.mp3',
                '/source/sounds/fight/male/get_hit_5.mp3',
                '/source/sounds/fight/male/get_hit_6.mp3',
                '/source/sounds/fight/male/get_hit_7.mp3',
            ],
            machine: [
                '/source/sounds/fight/machine/get_hit_0.mp3',
                '/source/sounds/fight/machine/get_hit_1.mp3',
                '/source/sounds/fight/machine/get_hit_2.mp3',
                '/source/sounds/fight/machine/get_hit_3.mp3',
            ]
        },
    },
    makeHit: {
        sex: {
            female: [
                'наносим удар',
            ],
            male: [
                'наносим удар',
            ],
            machine: [
                'наносим удар',
            ]
        },
    },
    common: {
        arena: [
            'фоновая музыка для арены'
        ],
        round: [
            'раунды'
        ],
        other: [
            'versus',
            'lets fight',
            'friendship',
            'well done',
            'exellent',
            'you suck',
            'choose your desteny',
            'chosen'
        ]
    }
}

export {
    soundLib
};