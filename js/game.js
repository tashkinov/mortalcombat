import {
    logs
} from './logs.js';
import {
    getRandom,
    currentDate,
    createElement
} from './common.js';
import {
    Player
} from './player.js';
import {
    playSound
} from './functions.js';

const playerChar1 = JSON.parse(localStorage.getItem('player1'));
const playerChar2 = JSON.parse(localStorage.getItem('player2'));

class Game {
    constructor(props) {}
    start() {
        const $arena = document.querySelector('.arenas');
        const $fightButton = document.querySelector('.button');
        const $formFight = document.querySelector('.control');
        const $chat = document.querySelector('.chat');
        $arena.insertAdjacentHTML('afterbegin', '<img class="letsFight" src="/source/fight.gif">')

        playSound('fight').play();

        const player1 = new Player({
            player: 1,
            name: playerChar1.name,
            hp: playerChar1.hp,
            alias: playerChar1.alias,
            sex: playerChar1.sex,
            createPlayer,
        })

        const player2 = new Player({
            player: 2,
            name: playerChar2.name,
            hp: playerChar2.hp,
            alias: playerChar2.alias,
            sex: playerChar2.sex,
            createPlayer,
        })

        const HIT = {
            head: 60,
            body: 45,
            foot: 40,
        }

        const ATTACK = ['head', 'body', 'foot'];

        async function createPlayer() {
            const $player = createElement('div', 'player' + this.player);
            const $progressbar = createElement('div', 'progressbar');
            const $life = createElement('div', 'life');
            const $name = createElement('div', 'name');
            const $character = createElement('div', 'character');
            const $img = createElement('img', this.alias);
            $arena.appendChild($player);
            $player.appendChild($progressbar);
            $progressbar.appendChild($life);
            $progressbar.appendChild($name);
            $player.appendChild($character);
            $character.appendChild($img);
            $name.innerHTML = `${this.name}`;
            $life.style.width = this.hp + '%';
            await this.getImage('stance').then(res => $img.src = res);
            return $player;
        }

        async function apendPlayer() {
            $arena.appendChild(await createPlayer.call(player1))
            $arena.appendChild(await createPlayer.call(player2))
        }
        apendPlayer();

        function enemyAttack() {
            const hit = ATTACK[getRandom(3) - 1];
            const defence = ATTACK[getRandom(3) - 1];
            return {
                value: getRandom(HIT[hit]),
                hit,
                defence,
            }
        }

        function playerAttack() {
            const attack = {};
            for (let item of $formFight) {
                if (item.checked && item.name === 'hit') {
                    attack.value = getRandom(HIT[item.value]);
                    attack.hit = item.value;
                }
                if (item.checked && item.name === 'defence') {
                    attack.defence = item.value;
                }
                item.checked = false;
            }
            return attack;
        }

        async function winnerCheck(nameWinner, nameLoser) {
            let $winTitle = createElement('div', 'loseTitle');
            if (nameWinner || nameLoser) {
                $formFight.classList.add('controlHide');
                $winTitle.innerHTML = nameWinner.name + ' wins';
                generateLogs('end', nameWinner, nameLoser)
                let winnerImage = document.querySelector(`.${nameWinner.alias}`);
                let loserImage = document.querySelector(`.${nameLoser.alias}`);
                await nameWinner.getImage('victory').then(res => winnerImage.src = res);
                await nameLoser.getImage('defeat').then(res => loserImage.src = res);
            } else {
                $winTitle.innerHTML = 'draw';
                generateLogs('draw')
            }
            $arena.appendChild($winTitle)
        }

        function showResult() {
            if (player2.hp === 0 || player1.hp === 0) {
                $fightButton.disabled = true;
                createReloadButton();
                let typeEnd = logs.end;
                generateLogs(logs.end[getRandom(typeEnd.length)], player1, player2);
            }

            if (player1.hp === 0 && player1.hp < player2.hp) {
                winnerCheck(player2, player1);
                playSound('wins', null, `${player2.alias}`).play();
            } else if (player2.hp === 0 && player2.hp < player1.hp) {
                winnerCheck(player1, player2);
                playSound('wins', null, `${player1.alias}`).play();
            } else if (player1.hp === 0 && player2.hp === 0) {
                winnerCheck();
            }

        }

        generateLogs('start', player1, player2);

        function generateLogs(type, player1, player2, damage) {
            switch (type) {
                case 'start':
                    const textStart = logs[type]
                        .replace('[player1]', player1.name)
                        .replace('[player2]', player2.name)
                        .replace('[time]', currentDate());
                    let elStart = `<p>${textStart}</p>`;
                    $chat.insertAdjacentHTML('afterbegin', elStart);
                    break;
                case 'end':
                    const texEnd = logs[type][getRandom(logs[type].length) - 1]
                        .replace('[playerLose]', player2.name)
                        .replace('[playerWins]', player1.name);
                    let elEnd = `<p>${texEnd}</p>`;
                    $chat.insertAdjacentHTML('afterbegin', elEnd);
                    break;
                case 'hit':
                    const textHit = logs[type][getRandom(logs[type].length) - 1]
                        .replace('[playerDefence]', player2.name)
                        .replace('[playerKick]', player1.name);
                    let elHit = `<p>${currentDate()} - ${textHit} -${damage} [${player2.hp}/100]</p>`;
                    $chat.insertAdjacentHTML('afterbegin', elHit);
                    break;
                case 'defence':
                    const textDefence = logs[type][getRandom(logs[type].length) - 1]
                        .replace('[playerDefence]', player1.name)
                        .replace('[playerKick]', player2.name);
                    let elDefence = `<p>${currentDate()} - ${textDefence}</p>`;
                    $chat.insertAdjacentHTML('afterbegin', elDefence);
                    break;
                case 'draw':
                    $chat.insertAdjacentHTML('afterbegin', logs.draw);
                    break;
            }
        }

        $formFight.addEventListener('submit', async function (e) {

            $formFight.classList.add('controlHide');

            setTimeout(() => {
                $formFight.classList.remove('controlHide');
            }, 2500);

            e.preventDefault();
            const enemy = enemyAttack();
            const player = playerAttack();
            let player2Image = document.querySelector(`.${player2.alias}`);
            let player1Image = document.querySelector(`.${player1.alias}`);

            setTimeout(async () => {
                switch (enemy.hit) {
                    case 'head':
                        await player2.getImage('kick_head').then(res => player2Image.src = res);
                        player2Image.classList.add('player2Hit');
                        setTimeout(async () => {
                            player2Image.classList.remove('player2Hit');
                            await player2.getImage('stance').then(res => player2Image.src = res);
                        }, 500);
                        break;
                    case 'body':
                        await player2.getImage('kick_body').then(res => player2Image.src = res);
                        player2Image.classList.add('player2Hit');
                        setTimeout(async () => {
                            player2Image.classList.remove('player2Hit');
                            await player2.getImage('stance').then(res => player2Image.src = res);
                        }, 500);
                        break;
                    case 'foot':
                        await player2.getImage('kick_foot').then(res => player2Image.src = res);
                        player2Image.classList.add('player2Hit');
                        setTimeout(async () => {
                            player2Image.classList.remove('player2Hit');
                            await player2.getImage('stance').then(res => player2Image.src = res);
                        }, 500)
                        break;
                }
            }, 1500);

            if (enemy.hit != player.defence) {
                setTimeout(async () => {
                    player1.changeHP(player.value);
                    player1.renderHP();

                    playSound('gotHit', `${player1.sex}`).play();
                    generateLogs('hit', player2, player1, player.value);

                }, 1500);


            } else {
                setTimeout(async () => {
                    generateLogs('defence', player1, player2);
                    await player1.getImage('block').then(res => player1Image.src = res);
                    playSound('block').play();
                }, 1500)
                setTimeout(async () => {
                    await player1.getImage('stance').then(res => player1Image.src = res);
                }, 2000)

            }

            switch (player.hit) {
                case 'head':
                    await player1.getImage('kick_head').then(res => player1Image.src = res);
                    player1Image.classList.add('player1Hit');
                    setTimeout(async () => {
                        player1Image.classList.remove('player1Hit');
                        await player1.getImage('stance').then(res => player1Image.src = res);
                    }, 500);
                    break;
                case 'body':
                    await player1.getImage('kick_body').then(res => player1Image.src = res);
                    player1Image.classList.add('player1Hit');
                    setTimeout(async () => {
                        player1Image.classList.remove('player1Hit');
                        await player1.getImage('stance').then(res => player1Image.src = res);
                    }, 500);
                    break;
                case 'foot':
                    await player1.getImage('kick_foot').then(res => player1Image.src = res);
                    player1Image.classList.add('player1Hit');
                    setTimeout(async () => {
                        player1Image.classList.remove('player1Hit');
                        await player1.getImage('stance').then(res => player1Image.src = res);
                    }, 500);
                    break;
            }

            if (player.hit != enemy.defence) {
                player2.changeHP(enemy.value);
                player2.renderHP();
                playSound('gotHit', `${player2.sex}`).play();
                generateLogs('hit', player1, player2, enemy.value);

            } else {

                generateLogs('defence', player2, player1);
                await player2.getImage('block').then(res => player2Image.src = res);
                playSound('block').play();
                setTimeout(async () => {
                    await player2.getImage('stance').then(res => player2Image.src = res);
                }, 500)
            }
            setTimeout(() => {
                showResult();
            }, 2500);

        })

        function createReloadButton() {
            const $reloadButtonDiv = createElement('div', 'reloadWrap');
            const $reloadButton = createElement('button', 'button');
            $reloadButtonDiv.appendChild($reloadButton)
            $reloadButton.innerHTML = 'Restart';
            $arena.appendChild($reloadButtonDiv);

            $reloadButton.addEventListener('click', () => {
                window.location.reload()
            })
        }
    };
}

export default Game;