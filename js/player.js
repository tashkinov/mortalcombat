class Player {
    constructor(props) {
        this.player = props.player;
        this.name = props.name;
        this.hp = props.hp;
        this.attack = props.attack;
        this.alias = props.alias;
        this.sex = props.sex;
    }
    elHP() {
        return document.querySelector('.player' + this.player + ' .life');
    }
    changeHP(damage) {
        this.hp = ((this.hp - damage) > 0) ? this.hp - damage : 0;
    }
    renderHP() {
        this.elHP().style.width = this.hp + '%';
    }

    async getImage(type) {
        let imageUrl = await fetch(`source/character/${this.alias}/${type}.gif`);
        if (imageUrl.status == 200) {
            return imageUrl.url;
        } else {
            imageUrl = await fetch(`source/character/${this.alias}/${type}.png`);
            return imageUrl.url;
        }
    }
}

export {
    Player
}
