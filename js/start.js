import {
    characters
} from './characters.js';
import {
    getRandom
} from './common.js';
import {
    Player
} from './player.js';
import {
    playSound
} from './functions.js';

const $root = document.querySelector('.main');
const $parent = document.querySelector('.parent');
const $player1 = document.querySelector('.player1Char');
const $player2 = document.querySelector('.player2Char');
const $vs = document.querySelector('.vs');

const createElement = (tag, className) => {
    const $tag = document.createElement(tag);
    if (className) {
        if (Array.isArray(className)) {
            className.forEach(item => {
                $tag.classList.add(item);
            })
        } else {
            $tag.classList.add(className);
        }
    }
    return $tag;
}

async function init() {
    localStorage.removeItem('player1');
    localStorage.removeItem('player2');
    let imgSrc = null;

    characters.forEach(item => {
        const char = new Player({
            name: item.name,
            alias: item.alias
        })

        const el = createElement('div', ['characterIcon', `div${item.id}`]);
        const img = createElement('img');

        el.addEventListener('mousemove', async () => {
            if (imgSrc === null) {
                await char.getImage('stance').then(res => imgSrc = res)
                const $img = createElement('img');
                $img.src = imgSrc;
                $player1.appendChild($img);
            }
        });

        el.addEventListener('mouseout', () => {
            if (imgSrc && !localStorage.player1) {
                imgSrc = null;
                $player1.innerHTML = '';
            }
        });

        el.addEventListener('click', async () => {
            localStorage.setItem('player1', JSON.stringify(item));
            playSound('chosen').play();

            setTimeout(() => {
                playSound('name', null, char.alias).play();
            }, 900);

            el.classList.add('active');

            const $player1ImgVs = createElement('img', 'player1Vs');
            let $player1ImgVsSrc;
            await char.getImage('vs').then(res => $player1ImgVsSrc = res);
            $player1ImgVs.src = $player1ImgVsSrc;
            $vs.appendChild($player1ImgVs);

            // GENERATE RANDOM PLAYER2 
            setTimeout(() => {
                player2Random();
            }, 2000);

            // VS START
            setTimeout(() => {
                $root.classList.add('up');
                $vs.classList.remove('down');
                playSound('vs').play();
                setTimeout(() => {
                    $vs.insertAdjacentHTML('afterbegin', '<div class="vs-animation"><img src="/source/vs.gif"></div>')
                }, 500);
            }, 4500);

            //GOTO ARENAS
            setTimeout(() => {
                window.location.pathname = 'arenas.html';
            }, 7000);
        });

        char.getImage('icon').then(res => img.src = res);
        img.alt = char.name;

        el.appendChild(img);
        $parent.appendChild(el);
    });

}

async function player2Random() {
    const playerChar1 = JSON.parse(localStorage.getItem('player1'));
    let index = getRandom(characters.length) - 1;

    while (playerChar1.name == characters[index].name) {
        index = getRandom(characters.length) - 1;
    }

    const characterEl = document.querySelector(`.div${index + 1}`);
    characterEl.classList.add('active', 'opposite');
    localStorage.setItem('player2', JSON.stringify(characters[index]));


    const char = new Player({
        name: characters[index].name,
        alias: characters[index].alias,
    })
    playSound('chosen').play();

    setTimeout(() => {
        playSound('name', null, char.alias).play();
    }, 900);

    let imgSrc = null;
    if (imgSrc === null) {
        await char.getImage('stance').then(res => imgSrc = res)
        const $img = createElement('img');
        $img.src = imgSrc;
        $player2.appendChild($img);
    }
    let $imgVs = createElement('img', 'player2Vs');
    let $imgVsCrc;
    await char.getImage('vs').then(res => $imgVsCrc = res);
    $imgVs.src = $imgVsCrc;
    $vs.appendChild($imgVs);
}

init();
